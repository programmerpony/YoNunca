package com.programmerpony.yonunca

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import io.ktor.client.*
import io.ktor.client.call.body
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.HttpResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPreferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.Theme_YoNunca)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreferences = getSharedPreferences("YoNunca", Context.MODE_PRIVATE)
        val darkMode = sharedPreferences.getBoolean("dark_mode",false)
        AppCompatDelegate.setDefaultNightMode(if (darkMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        val updateOnStart = sharedPreferences.getBoolean("update_on_start",true)
        if (updateOnStart) CoroutineScope(Dispatchers.IO).launch {
            update()
        }

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = ""
        toolbar.inflateMenu(R.menu.settings_menu)
        toolbar.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.settings) {
                val intent = Intent(this@MainActivity, SettingsActivity::class.java)
                startActivity(intent)
            }
            false
        }

        findViewById<Button>(R.id.buttonNormal).setOnClickListener { startGame(0) }
        findViewById<Button>(R.id.buttonSpicy).setOnClickListener { startGame(1) }
        findViewById<Button>(R.id.buttonMixed).setOnClickListener { startGame(2) }
    }

    private fun startGame(mode : Int) {
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra("mode", mode)
        startActivity(intent)
    }

    private suspend fun update() {
        val client = HttpClient(CIO)
        try {
            val response : HttpResponse = client.get("https://yonunca.programmerpony.com/assets/frases.json")
            Log.d("response",response.body())
            withContext(Main) {
                sharedPreferences.edit().putString("frases",response.body()).apply()
            }
        } catch (e: Exception) {
            return
        }
    }
}