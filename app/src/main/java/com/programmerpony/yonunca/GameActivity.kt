package com.programmerpony.yonunca

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.text.HtmlCompat
import org.json.JSONArray
import org.json.JSONObject

class GameActivity : AppCompatActivity() {

    private lateinit var listaFrases : List<Any>
    private var index = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val toolbar : Toolbar = findViewById(R.id.toolbarGame)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {finish()}

        val sharedPreferences = getSharedPreferences("YoNunca", Context.MODE_PRIVATE)
        val frases = JSONObject(
            sharedPreferences.getString(
                "frases", /* Frases que vienen con la app */
                "{ \"normal\": [ \"Yo nunca he saltado en paracaídas.\", \"Yo nunca he mentido jugando a Yo Nunca.\", \"Yo nunca le he robado dinero a mis padres.\", \"Yo nunca me he levantado con resaca.\", \"Yo nunca me he meado encima.\", \"Yo nunca he roto la pantalla del móvil de otra persona.\", \"Yo nunca he roto la pantalla de mi móvil.\", \"Yo nunca he querido besar a alguien pero no lo he hecho por miedo.\", \"Yo nunca he tenido que huir de la policía.\", \"Yo nunca he conducido un coche sin tener carnet.\", \"Yo nunca he sido comunista.\", \"Yo nunca he sido anarquista.\", \"Yo nunca he sido un pagafantas.\", \"Yo nunca he llorado viendo una película de dibujos animados.\", \"Yo nunca he salido en la tele.\", \"Yo nunca he salido en la radio.\", \"Yo nunca he salido en un periódico.\", \"Yo nunca he stalkeado a alguien.\", \"Yo nunca he participado en un karaoke.\", \"Yo nunca he estado borracho.\", \"Yo nunca he conocido a alguien famoso.\", \"Yo nunca he tonteado con varias personas a la vez.\", \"Yo nunca he pesado la fruta, le he pegado el ticket y luego he metido más.\", \"Yo nunca me he tropezado con los auriculares.\", \"Yo nunca he hecho spoilers.\", \"Yo nunca he tenido una relación de más de un año.\", \"Yo nunca he sido expulsado de clase.\", \"Yo nunca he sido expulsado del colegio.\", \"Yo nunca he sido expulsado del instituto.\", \"Yo nunca me he peleado a puñetazos.\", \"Yo nunca he meado en la playa.\", \"Yo nunca he meado en la piscina.\", \"Yo nunca he intentado bailar como Michael Jackson.\", \"Yo nunca he estado despierto durante más de 24 horas.\", \"Yo nunca he apagado la luz del pasillo y he salido corriendo.\", \"Yo nunca he pensado en operarme los pechos.\", \"Yo nunca he dudado de mi orientación sexual.\", \"Yo nunca he dudado de mi género.\", \"Yo nunca me he dormido en clase.\", \"Yo nunca me he dormido en el trabajo.\", \"Yo nunca me he despertado después de una fiesta en otra ciudad.\", \"Yo nunca he hablado solo en voz alta por la calle.\", \"Yo nunca he llorado en el cine.\", \"Yo nunca he sido youtuber.\", \"Yo nunca he sido infiel.\", \"Yo nunca he robado en un supermercado.\", \"Yo nunca me he sometido a una operación.\", \"Yo nunca me he sometido a una cirugía estética.\", \"Yo nunca me he roto un hueso.\", \"Yo nunca he dado un \\\"me gusta\\\" por accidente mientras stalkeaba a alguien.\", \"Yo nunca he repetido curso.\", \"Yo nunca he abrazado a un árbol.\", \"Yo nunca he hecho trampa en un examen.\", \"Yo nunca he estado inconsciente.\", \"Yo nunca me he emborrachado con mi familia.\", \"Yo nunca he llevado la camiseta del revés sin darme cuenta.\", \"Yo nunca he estado en otro país.\", \"Yo nunca he viajado a otro país.\", \"Yo nunca me he olido la mano después de rascarme el culo.\", \"Yo nunca he escrito un poema a alguien.\", \"Yo nunca me he escapado de casa.\", \"Yo nunca he ido a una boda por la comida.\", \"Yo nunca he estado más de 3 días sin ducharme.\", \"Yo nunca he dicho \\\"te quiero\\\" sin querer.\", \"Yo nunca he salido a la calle sin ropa interior.\", \"Yo nunca me he tropezado por estar mirando el móvil.\", \"Yo nunca he fumado tabaco.\", \"Yo nunca he fumado cannabis.\", \"Yo nunca he liado un porro.\", \"Yo nunca he rechazado un beso.\", \"Yo nunca he sido engañado por mi pareja.\", \"Yo nunca he dicho que me encantaba un regalo y después lo he devuelto. \", \"Yo nunca he fingido amar un grupo de música cuando solo me sé el nombre.\", \"Yo nunca he llamado mamá a mi profesora.\", \"Yo nunca he usado sandalias con calcetines.\", \"Yo nunca he intentado adivinar la contraseña de alguien.\", \"Yo nunca he hecho un \\\"simpa\\\".\", \"Yo nunca me he montado en una moto.\", \"Yo nunca he disparado un arma de fuego.\", \"Yo nunca he abierto una puerta de una patada.\", \"Yo nunca he tirado de una puerta con un cartel de \\\"empuje\\\".\", \"Yo nunca he sacado una matrícula de honor.\", \"Yo nunca he molestado a un oficial del ejército.\", \"Yo nunca he intentado bailar como Michael Jackson.\", \"Yo nunca he probado la comida para gatos.\", \"Yo nunca he probado la comida para perros.\", \"Yo nunca he hecho <i>puenting</i>.\", \"Yo nunca he comido sushi.\", \"Yo nunca he sido perseguido por un perro.\", \"Yo nunca he comprado un producto de Apple.\", \"Yo nunca he sido arrestado.\", \"Yo nunca he sido detenido por la policía.\", \"Yo nunca he insultado a alguien en un idioma que no entiende.\", \"Yo nunca me he tropezado y he tirado la comida al suelo.\", \"Yo nunca he sido expulsado de un bar.\", \"Yo nunca me he saltado tres o más semáforos en rojo seguidos.\", \"Yo nunca me he rapado al cero.\", \"Yo nunca me he bañado en una fuente.\", \"Yo nunca he hecho un muñeco de nieve.\", \"Yo nunca me he creado una cuenta en una página de citas.\", \"Yo nunca he usado Tinder.\", \"Yo nunca he mezclado Mentos y Coca-Cola.\", \"Yo nunca me he grabado cantando.\", \"Yo nunca he visto Titanic entera.\", \"Yo nunca he limpiado mi habitación metiéndolo todo en el armario.\", \"Yo nunca he jugado con una <i>ouija</i>.\", \"Yo nunca he bebido antes de cumplir los 18.\", \"Yo nunca he fingido tener pareja.\", \"Yo nunca he borrado mi historial de navegación.\", \"Yo nunca me he aprendido la coreografía de una canción de <i>k-pop</i>.\", \"Yo nunca he ido en pijama y zapatillas por la calle.\", \"Yo nunca me he teñido el pelo.\" ], \"picante\": [ \"Yo nunca he hecho un trío.\", \"Yo nunca he participado en una orgía.\", \"Yo nunca he tenido un follamigo.\", \"Yo nunca he tenido sexo con alguien 10 años mayor que yo.\", \"Yo nunca le he pedido a mi pareja que me asfixie teniendo sexo.\", \"Yo nunca he sido atado teniendo sexo.\", \"Yo nunca me he tirado un pedo vaginal durante el acto sexual.\", \"Yo nunca he hecho sexo oral a otra persona.\", \"Yo nunca he recibido sexo oral.\", \"Yo nunca he tenido sexo con una MILF.\", \"Yo nunca he tenido sexo con la pareja de un amigo.\", \"Yo nunca he tenido sexo con alguno de los presentes.\", \"Yo nunca he visto una película porno con alguien.\", \"Yo nunca he llegado al orgasmo varias veces durante una sesión de sexo.\", \"Yo nunca he pensado en comprarme un Satisfyer.\", \"Yo nunca he hecho un trío homosexual.\", \"Yo nunca he tenido sexo en un probador.\", \"Yo nunca he tenido sexo en el baño de un restaurante.\", \"Yo nunca me he empalmado en clase.\", \"Yo nunca he pagado por contenido pornográfico.\", \"Yo nunca he fantaseado con alguien aquí presente.\", \"Yo nunca me he masturbado para alguien.\", \"Yo nunca me he masturbado en frente de gente.\", \"Yo nunca he tenido sexo en el instituto.\", \"Yo nunca me he masturbado en un sitio público.\", \"Yo nunca he follado en un sitio público.\", \"Yo nunca he hecho un beso negro.\", \"Yo nunca he recibido un beso negro.\", \"Yo nunca me he equivocado de nombre durante el sexo.\", \"Yo nunca me he masturbado en grupo.\", \"Yo nunca he tenido sexo en la playa.\", \"Yo nunca me he masturbado en la playa.\", \"Yo nunca he ido a una playa nudista.\", \"Yo nunca me he masturbado pensando en alguno de los presentes.\", \"Yo nunca he tenido sexo en un balcón.\", \"Yo nunca he deseado a alguno de los presentes.\", \"Yo nunca me he liado con alguno de los presentes.\", \"Yo nunca he participado en un gang bang.\", \"Yo nunca he estado saliendo con alguien solo por sexo.\", \"Yo nunca he tenido semen en la cara.\", \"Yo nunca he invitado a alguien a ver una película con doble intención.\", \"Yo nunca he usado comida durante el sexo.\", \"Yo nunca he rechazado a alguien que quería tener sexo conmigo.\", \"Yo nunca he tenido sexo en la cocina.\", \"Yo nunca he tenido sexo con varias personas el mismo día.\", \"Yo nunca me he hecho fotos de mis genitales.\", \"Yo nunca me he grabado teniendo sexo.\", \"Yo nunca me he grabado masturbándome.\", \"Yo nunca he olido las bragas de alguien.\", \"Yo nunca me he masturbado con una fruta.\", \"Yo nunca he hecho juegos de rol sexuales.\", \"Yo nunca he enviado nudes.\", \"Yo nunca me he arrepentido de enviar nudes.\", \"Yo nunca me he excitado por la calle.\", \"Yo nunca le he mirado el culo a un profesor.\", \"Yo nunca le he mirado el culo a una profesora.\", \"Yo nunca he afeitado el vello púbico de otra persona.\", \"Yo nunca me he metido un dedo por el culo.\", \"Yo nunca me he metido más de un dedo por el culo.\", \"Yo nunca he probado mis propios fluidos.\", \"Yo nunca he fingido un orgasmo.\", \"Yo nunca me he tomado la píldora del día después.\", \"Yo nunca he tenido un <i>sugar daddy</i>.\", \"Yo nunca he tenido una <i>sugar mommy</i>.\", \"Yo nunca he jugado al juego de la botella.\", \"Yo nunca he visto una película porno entera.\", \"Yo nunca he usado lencería.\", \"Yo nunca me he hecho un <i>piercing</i> en los genitales.\", \"Yo nunca me he hecho un <i>piercing</i> en un pezón.\", \"Yo nunca he tenido un sueño erótico con alguien que no debería.\", \"Yo nunca he sido descubierto masturbándome.\" ] } "
            ).toString()
        )

        listaFrases = when(intent.getIntExtra("mode", 0)) {
            0 -> frases.getJSONArray("normal").toList().shuffled()
            1 -> frases.getJSONArray("picante").toList().shuffled()
            else -> {
                val arr = frases.getJSONArray("normal")
                for (i in 0 until frases.getJSONArray("picante").length()) {
                    arr.put(frases.getJSONArray("picante").getString(i))
                }
                arr.toList().shuffled()
            }
        }

        next()

        findViewById<Button>(R.id.yoBebo).setOnClickListener { next() }
        findViewById<Button>(R.id.yoPaso).setOnClickListener { next() }

    }

    private fun next() {
        if (index >= listaFrases.size) index = 0
        val frase : TextView = findViewById(R.id.frase)
        frase.text = HtmlCompat.fromHtml((listaFrases[index].toString()).replace("Yo nunca", "<b>Yo nunca</b>"),HtmlCompat.FROM_HTML_MODE_LEGACY)
        index++
    }

    private fun JSONArray.toList(): List<Any> {
        val list = mutableListOf<Any>()
        for (i in 0 until this.length()) {
            var value: Any = this[i]
            when (value) {
                is JSONArray -> value = value.toList()
            }
            list.add(value)
        }
        return list
    }
}