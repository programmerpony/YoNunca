package com.programmerpony.yonunca

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.mikepenz.aboutlibraries.LibsBuilder

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val data: Uri? = intent?.data
        if (data.toString() == "yonunca://licencias") {
            LibsBuilder()
                .withActivityTitle(getString(R.string.licencias))
                .start(this)
            finish()
        }

        val switchDarkMode : SwitchCompat = findViewById(R.id.switchDarkMode)
        val switchUpdate : SwitchCompat = findViewById(R.id.switchUpdate)

        val sharedPreferences = getSharedPreferences("YoNunca", Context.MODE_PRIVATE)
        var darkMode = sharedPreferences.getBoolean("dark_mode", false)
        var updateOnStart = sharedPreferences.getBoolean("update_on_start", true)
        if (darkMode) switchDarkMode.isChecked = true
        if (updateOnStart) switchUpdate.isChecked = true

        val toolbar : Toolbar = findViewById(R.id.toolbarSettings)
        toolbar.title = getString(R.string.settings)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, if (darkMode) R.color.white else R.color.darkGrey))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {finish()}

        switchDarkMode.setOnCheckedChangeListener { _, b ->
            darkMode = b
            sharedPreferences.edit().putBoolean("dark_mode", darkMode).apply()
            AppCompatDelegate.setDefaultNightMode(if (darkMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
        }

        switchUpdate.setOnCheckedChangeListener { _, b ->
            updateOnStart = b
            sharedPreferences.edit().putBoolean("update_on_start", updateOnStart).apply()
        }

        findViewById<TextView>(R.id.textViewTerms).movementMethod = LinkMovementMethod.getInstance()
        findViewById<Button>(R.id.sugerirFraseNueva).setOnClickListener {
            val uri = Uri.parse("mailto:contact@programmerpony.com?subject=Nueva frase Yo Nunca&body=Yo nunca")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

    }
}